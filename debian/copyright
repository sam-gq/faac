Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FAAC
Source: https://github.com/knik0/faac
Disclaimer: This package contains software that has been originally
 developed in the course of development of the MPEG-2 NBC/MPEG-4 Audio
 standard ISO/IEC 13818-7, 14496-1,2 and 3. This software module is an
 implementation of a part of one or more MPEG-2 NBC/MPEG-4 Audio tools
 as specified by the MPEG-2 NBC/MPEG-4 Audio standard.  While the
 resulting programs and binaries result in redistributable software, the
 effects on application packages derived from this library remain
 unclear. Therefore, the Debian 'non-free' section is an adequate place
 until the licenses for the problematic parts have been clarified by
 relicensing or reimplementation.
 .
 This package can legitimately and technically be auto-built, its license
 does not disapprove of automated building nor does it impose restrictions
 related to architecture or distribution.

Files: *
Copyright: © 1999-2001, Menno Bakker
           © 2002-2017, Krzysztof Nikiel
           © 2004, Dan Villiom P. Christiansen
           © 2004, Volker Fischer
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: frontend/getopt.*
Copyright: © 1987-1993, Free Software Foundation, Inc.
License: GPL-2+

Files: frontend/mp4write.*
       libfaac/huff2.*
       libfaac/huffdata.*
       libfaac/quantize.*
       libfaac/stereo.*
Copyright: © 2017, Krzysztof Nikiel
License: GPL-3+

Files: libfaac/bitstream.*
       libfaac/channels.c
       libfaac/filtbank.c
       libfaac/tns.*
Copyright: © 1996-1997
License: other
 This software module was originally developed in the course of
 development of the MPEG-2 NBC/MPEG-4 Audio standard ISO/IEC 13818-7,
 14496-1,2 and 3. This software module is an implementation of a part of
 one or more MPEG-2 NBC/MPEG-4 Audio tools as specified by the MPEG-2
 NBC/MPEG-4 Audio standard. ISO/IEC gives users of the MPEG-2 NBC/MPEG-4
 Audio standards free license to this software module or modifications
 thereof for use in hardware or software products claiming conformance
 to the MPEG-2 NBC/ MPEG-4 Audio standards. Those intending to use this
 software module in hardware or software products are advised that this
 use may infringe existing patents. The original developer of this
 software module and his/her company, the subsequent editors and their
 companies, and ISO/IEC have no liability for use of this software
 module or modifications thereof in an implementation. Copyright is not
 released for non MPEG-2 NBC/MPEG-4 Audio conforming products. The
 original developer retains full right to use the code for his/her own
 purpose, assign or donate the code to a third party and to inhibit
 third party from using the code for non MPEG-2 NBC/MPEG-4 Audio
 conforming products. This copyright notice must be included in all
 copies or derivative works.

Files: libfaac/kiss_fft/*
Copyright: © 2003-2004, Mark Borgerding
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the author nor the names of any contributors may be used to endorse
   or promote products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: © 2005-2018, Fabian Greffrath <fabian@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
